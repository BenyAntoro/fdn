import socket
import flask
from datetime import datetime
from flask import Flask
from flask import request
from flask import jsonify

app = Flask(__name__)
@app.route('/')
def hello():
    return 'Hello'
@app.route('/time=now')
def date_time():
    date = datetime.now()
    return str(date)
@app.route("/whoami=1", methods = ["GET"])
def whoami():
   return jsonify({
      'ip': request.remote_addr
   }), 200

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
